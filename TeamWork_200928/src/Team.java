import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {

	public static void main(String[] args) throws IOException {
		// 导入配置信息：获取properties文件中的url和cookie
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("resources/config.properties")); // 文件位置：相对路径引入
		} catch (Exception e) {
			e.printStackTrace(); // 出错处理
		}
		
		// 创建列表
		ArrayList<String> baseList = new ArrayList<String>();
		
		// 创建document文件，通过url和cookie引入html网页的数据
		Document document = (Document) Jsoup.connect(prop.getProperty("url")).header("cookie", prop.getProperty("cookie")).get();
		if (document != null) {
			// 通过interaction-row类获取数据
			Elements activDivs = ((Element) document).getElementsByClass("interaction-row");
			for (int i = 0; i < activDivs.size(); i++) {
				if (activDivs.get(i).toString().contains("课堂完成")) {
					String urlString = activDivs.get(i).attr("data-url").toString();
					baseList.add(urlString);
				}
			}
		} else { // url和cookie正常，但未获取到网页
			System.out.println("出错啦！");
		}
		// 列表赋值
		ArrayList<Student> newStudentList=stuList(baseList);
		Collections.sort(newStudentList, new Student());
		// 数据写入文件
		write(newStudentList);
	}

	public static ArrayList<Student> stuList(ArrayList<String> baseList) throws IOException {
		// 获取另一个配置信息的对象prop1
		Properties prop1 = new Properties();
		try {
			prop1.load(new FileInputStream("resources/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 列表
		ArrayList<Document> baseActives = new ArrayList<Document>(baseList.size());
		ArrayList<Student> studentList = new ArrayList<>();
		
		for (int i = 0; i < baseList.size(); i++) {
			Document document1 = (Document) Jsoup.connect(baseList.get(i)).header("cookie", prop1.getProperty("cookie")).get();
			baseActives.add(document1);
		}
		
		for (int j = 0; j < baseActives.size(); j++) {
			Elements baseStuDivs = ((Element) baseActives.get(j)).getElementsByClass("homework-item");
			for (int k = 0; k < baseStuDivs.size(); k++) {
				try {
					Student stu = new Student();
					stu.setId(baseStuDivs.get(k).child(0).child(1).child(1).text().toString());
					stu.setName(baseStuDivs.get(k).child(0).child(1).child(0).text().toString());
					String score = baseStuDivs.get(k).child(3).child(1).child(1).text();
					// 未评分的学生
					if (baseStuDivs.get(k).child(3).child(0).child(1).text().contains("尚无评分")) {
						stu.setScore(0.0);
					}
					// 未提交的学生
					else if (baseStuDivs.get(k).child(1).child(0).text().contains("未提交")) {
						stu.setScore(0.0);
					} else {
						stu.setScore(Double.parseDouble(score.substring(0, score.length() - 2)));
					}
					studentList.add(stu);
				} catch (Exception e) {
				}
			}
		}
		ArrayList<Student> newStudentList = new ArrayList<>();
		Double totalScore;
		for (int i = 0; i < studentList.size(); i++) {
			totalScore = 0.0;
			Student student = new Student();
			for (int j = i + 1; j < studentList.size(); j++) {
				if (studentList.get(i).getName().contains(studentList.get(j).getName())) {
					totalScore += studentList.get(j).getScore();
					studentList.remove(j);
				}
			}
			student.setId(studentList.get(i).getId());
			student.setName(studentList.get(i).getName());
			student.setScore(totalScore);
			newStudentList.add(student);
		}
		return newStudentList;
	}

	public static void write(ArrayList<Student> newStudentList) throws FileNotFoundException {
		File file = new File("score.txt");
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file), true);
		double ave = 0.0;
		for (int j = 0; j < newStudentList.size(); j++) {
			ave += newStudentList.get(j).getScore();
		}
		ave = ave / newStudentList.size();
		printWriter.println("最高经验值" + newStudentList.get(0).getScore() + ",最低经验值"
				+ newStudentList.get(newStudentList.size() - 1).getScore() + ",平均经验值" + ave);
		for (int i = 0; i < newStudentList.size(); i++) {
			printWriter.println(newStudentList.get(i).toString());
		}
		printWriter.close();
	}
}